﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using System.Diagnostics;
using System.Runtime;

//Benchmark-Resultate in Sekunden, Durchschnitt der 3 besten Resultate aus jeweils 10 Messungen
//
//Vergleich 1)
//=============
//- keine Einstellung in App.config
//- explizites Aufrufen von GC.Collect(...): mode = forced / optimized, blocking = true / false
//
//LatencyMode                 GC.Collect(2, GCCollectionMode.forced, true)    GC.Collect(2, GCCollectionMode.optimized, true)    
//---------------------------------------------------------------------------------------------------------------------------
//Batch                       9.33                                            9.5
//Interactive                 9.29                                            9.39
//LowLatency                  7.68                                            7.67
//SustainedLowLatency         9.22                                            9.34
//
//LatencyMode                 GC.Collect(2, GCCollectionMode.forced, false)    GC.Collect(2, GCCollectionMode.optimized, false)    
//-----------------------------------------------------------------------------------------------------------------------------
//Batch                       9.35                                             9.48
//Interactive                 9.26                                             9.35
//LowLatency                  (dauert ewig)                                    (dauert ewig)
//SustainedLowLatency         9.29                                             9.39

//Vergleich 2)
//=============
//- Einstellung in App.config: gcConcurrent enabled = true / false
//- kein explizites Aufrufen von GC.Collect(...)
//
//LatencyMode                 gcConcurrent enabled=true    
//-----------------------------------------------------
//Batch                       9.72
//Interactive                 9.51          (ist Standard LatencyMode)
//LowLatency                  7.56
//SustainedLowLatency         9.38
//
//LatencyMode                 gcConcurrent enabled=false
//------------------------------------------------------
//Batch                       9.6           (ist Standard LatencyMode)
//Interactive                 9.58
//LowLatency                  7.58
//SustainedLowLatency         (kann nicht als LatencyMode gesetzt werden --> Batch)


namespace GarbageCollectorConsole
{
    class Program
    {
        static void Main(string[] args)
        {
            Stopwatch watch = new Stopwatch();
            long f = 0;

            // alter Wert von LatencyMode speichern und ausgeben
            GCLatencyMode oldMode = GCSettings.LatencyMode;
            Console.WriteLine("Old LatencyMode: {0}", oldMode.ToString());


            try
            {
                // Werte von LatencyMode zum Vergleich setzen und ausgeben
                GCSettings.LatencyMode = GCLatencyMode.Batch;
                //GCSettings.LatencyMode = GCLatencyMode.Interactive;
                //GCSettings.LatencyMode = GCLatencyMode.LowLatency;
                //GCSettings.LatencyMode = GCLatencyMode.SustainedLowLatency;
                Console.WriteLine("New LatencyMode: {0}", GCSettings.LatencyMode.ToString());

                // Start-Zeit
                watch.Start();

                // Speicher allozieren ohne Referenzen
                AllocateMemory();

                // Explizites Garbage Collecting
                GC.Collect(2, GCCollectionMode.Forced, true);
                //GC.Collect(2, GCCollectionMode.Forced, false);
                //GC.Collect(2, GCCollectionMode.Optimized, true);
                //GC.Collect(2, GCCollectionMode.Optimized, false);

                // Berechnen von Fibonacci(40)
                f = CalculateFibonacci(40);

                // End-Zeit
                watch.Stop();
            }
            finally
            {
                // Wert von LatencyMode zurücksetzen
                GCSettings.LatencyMode = oldMode;
            }

            // benötigte Zeit ausgeben
            string time = string.Format("\n\t{0} minutes\n\t{1} seconds\n\t{2} milli seconds", watch.Elapsed.Minutes, watch.Elapsed.Seconds, watch.Elapsed.Milliseconds);
            Console.WriteLine("Elapsed Time: {0}", time);
            Console.WriteLine("Fibonacci(40) = {0}", f);


            Console.ReadKey();

        }

        // 32'768'000 mal ein 16ByteObject allozieren und dann die Referenzen löschen
        private static void AllocateMemory()
        {
            var list = new List<_16ByteObject>();
            //var tmp = new _16ByteObject();

            // 500 * 1MB = 500MB
            for (int i = 0; i < 500; i++)
            {
                // 1024 * 1KB = 1MB
                for (int j = 0; j < 1024; j++)
                {
                    // 64 * 16Byte = 1KB
                    for (int k = 0; k < 64; k++)
                    {
                        //tmp = new _16ByteObject();
                        list.Add(new _16ByteObject());
                    }
                }
            }
            list = null;
        }

        // Berechnen von Fibonacci ohne Speichern von Zwischenresultaten
        private static long CalculateFibonacci(int n)
        {
            if (n == 0)
            {
                return 0;
            }
            if (n == 1)
            {
                return 1;
            }
            return CalculateFibonacci(n-1) + CalculateFibonacci(n-2);
        }
    }
}
