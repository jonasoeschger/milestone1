﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace Fhnw.Ecnf.RoutePlanner.RoutePlannerLib
{
    public class City
    {

        public City() { }

        public City(string name, string country, int population, double latitude, double longitude) {
            Name = name;
            Country = country;
            Population = population;
            Location = new WayPoint(name, latitude, longitude);
        }

        // Lab 9 (11): Task 1)
        // -------------------
        [XmlIgnore]
        public int Index { get; set; }
        // -------------------

        public string Name { get; set; }

        public string Country { get; set; }
        
        public int Population { get; set; }

        public WayPoint Location { get; set; }

        public override Boolean Equals(object obj)
        {
            // If parameter is null return false.
            if (obj == null)
            {
                return false;
            }

            // If parameter cannot be cast to Point return false.
            City otherCity = obj as City;
            if ((System.Object)otherCity == null)
            {
                return false;
            }

            // Return true if the fields match:
            return (this.Name == otherCity.Name) && (this.Country == otherCity.Country);
        }

        public override int GetHashCode()
        {
            return Name.GetHashCode() ^ Country.GetHashCode();
        }

    }
}
