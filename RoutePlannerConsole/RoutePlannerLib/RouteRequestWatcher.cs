﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fhnw.Ecnf.RoutePlanner.RoutePlannerLib
{
    // Lab 3: Task 2c)
    // ---------------
    public class RouteRequestWatcher
    {

        // dictionary storing number of routes requested to some city
        private Dictionary<string, int> toCityDictionary;

        // event handler to observe Routes.FindShortestRouteBetween
        private Routes.RouteRequestHandler handler;

        public RouteRequestWatcher()
        {
            toCityDictionary = new Dictionary<string, int>();
            handler = new Routes.RouteRequestHandler(PrintAndStoreRouteRequestCounts);
        }

        // return event handler to observe Routes.FindShortestRouteBetween
        public Routes.RouteRequestHandler LogRouteRequests
        {
            get { return handler; }
        }

        // for every requested route, store how often a route to a destination has been requested and log to console
        private void PrintAndStoreRouteRequestCounts(Object sender, RouteRequestEventArgs eventArgs)
        {

            // store count to dictionary
            int requestCount = 0;
            string toCityName = eventArgs.ToCity.Name;
            if (toCityDictionary.ContainsKey(toCityName))
            {
                requestCount = toCityDictionary[toCityName] + 1;
                toCityDictionary[toCityName] = requestCount;
            }
            else
            {
                requestCount = 1;
                toCityDictionary.Add(toCityName, requestCount);
            }
            
            // logging
            Console.WriteLine("\nCurrent Request State");
            Console.WriteLine("=====================");
            foreach (KeyValuePair<string, int> entry in toCityDictionary)
            {
                Console.WriteLine("ToCity: {0} has been requested {1} times", entry.Key, entry.Value);
            }

        }

        // return how often a route to a destination has been requested
        public int GetCityRequests(string cityName)
        {
            if (toCityDictionary.ContainsKey(cityName))
            {
                return toCityDictionary[cityName];
            }
            else
            {
                return 0;
            }
        }

    }
}
