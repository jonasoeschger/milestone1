﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Reflection;

namespace Fhnw.Ecnf.RoutePlanner.RoutePlannerLib.Util
{
    public class Deserializer<T>
    {

        public int IndexOffset { get; private set; }

        public T deserialize(string[] input)
        {
            Boolean rootObjectCreated = false;
            IndexOffset = 0;
            string tmp;
            string[] tmpArray;
            Type type = null;
            PropertyInfo propertyInfo = null;

            // Output-Objekt mit default Wert "initialisieren"
            T t = default(T);

            // Input-String zeilenweise verarbeiten
            for (int i = 0; i < input.Length; i++)
            {
                // Offset: wenn dieser Deserializer "rekursiv" aufgerufen wurde
                // soll der Caller-Deserializer dort weitermachen, wo hier aufgehört wurde
                IndexOffset = i;

                tmp = input[i];
                if (tmp.StartsWith("Instance of"))
                {
                    tmpArray = tmp.Split(' ');
                    if (tmpArray.Length == 3)
                    {
                        // Start eines Objektes gefunden --> Typ auslesen

                        type = Type.GetType(tmpArray[2]);
                        if (type == null)
                        {
                            // Für Typen aus einem anderen Assembly reicht Type.GetType(..) nicht!
                            foreach (var a in AppDomain.CurrentDomain.GetAssemblies())
                            {
                                type = a.GetType(tmpArray[2]);
                                if (type != null)
                                    break;
                            }
                        }

                        if (rootObjectCreated)
                        {
                            // Es handelt sich um den Start eines Objektes,
                            // welches ein Property des zu serialisierenden "Root-Objekts" ist

                            if (propertyInfo != null)
                            {
                                // Ein "allgemeiner" Deserializer soll das Property-Objekt deserialisieren
                                Deserializer<object> myDeserializer = new Deserializer<object>();

                                // Der Input für den neuen Deserializer enthält den bereits verarbeiteten Teil nicht!
                                String[] newInput = new String[input.Length - i];
                                Array.Copy(input, i, newInput, 0, newInput.Length);

                                // Der Rückgabetyp des neuen Deserializers ist 'object' --> in richtigen Typen des Properties konvertieren!
                                var convertedValue = Convert.ChangeType(myDeserializer.deserialize(newInput), type);

                                // Hier soll da weiter deserialisiert werden, wo der neue Deserializer aufgehört hat
                                i = i + myDeserializer.IndexOffset;

                                // deserialisiertes Property-Objekt setzen
                                propertyInfo.SetValue(t, convertedValue, null);

                            }
                            else
                            {
                                // To Do: throw exception: error parsing string
                            }
                        }
                        else
                        {
                            // Es handelt sich um den Start des eigentlichen zu serialisierenden "Root-Objekts"
                            // --> "Root-Objekt" instanzieren
                            t = (T) Activator.CreateInstance(type);
                            rootObjectCreated = true;
                        }

                    }
                    else
                    {
                        // To Do: throw exception: error parsing string
                    }
                }
                else if (tmp.Equals("End of instance"))
                {
                    // Ende des eigentlichen zu serialisierenden "Root-Objekts"
                    // "break" könnte auch weggelassen werden, wenn der Input-String sowieso zu Ende ist...
                    break;
                }
                else
                {
                    // Ein Property des zu serialisierenden "Root-Objekts"

                    tmpArray = tmp.Split('=');
                    if (tmpArray.Length == 2)
                    {
                        // Es handelt sich um ein Property vom Typ string/int/double

                        // Anführungs- und Schlusszeichen (bei Strings) entfernen
                        string value = tmpArray[1].Replace("\"", "");

                        // Herausfinden, ob string oder int oder double und dann den Wert des Properties setzen
                        // Bei double sichergehen, dass sowohl '.' als auch ',' als Dezimalsymbol erkannt wird
                        int intValue;
                        Boolean isInteger = int.TryParse(value, out intValue);
                        double doubleValue;
                        Boolean isDouble = double.TryParse(value, System.Globalization.NumberStyles.Float, System.Globalization.CultureInfo.InvariantCulture, out doubleValue);                        
                        if (isInteger)
                        {
                            type.GetProperty(tmpArray[0]).SetValue(t, intValue);
                        }
                        else if (isDouble)
                        {
                            type.GetProperty(tmpArray[0]).SetValue(t, doubleValue);
                        }
                        else
                        {
                            type.GetProperty(tmpArray[0]).SetValue(t, value);
                        }
                    }
                    else
                    {
                        // '... is a nested object' gefunden --> Mit dem nächsten "Instance of..." 
                        // soll ein neuer Deserializer dieses Property deserialisieren und dann das deserialisierte Objekt gestzt werden
                        tmpArray = tmp.Split(' ');
                        if (tmpArray.Length > 0)
                        {
                            propertyInfo = type.GetProperty(tmpArray[0]);
                        }
                        else
                        {
                            // To Do: throw exception: error parsing string
                        }
                    }
                }
                    
            }

            // deserialisiertes Objekt zurückgeben
            return t;

        }

    }
}
