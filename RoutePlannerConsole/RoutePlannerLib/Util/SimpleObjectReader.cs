﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace Fhnw.Ecnf.RoutePlanner.RoutePlannerLib.Util
{
    public class SimpleObjectReader
    {

        // Deserializer
        private Deserializer<City> deserializer;

        // Reader
        private StringReader streamReader;

        // Indizes geben an, welche Teile des Input-Strings zu einzelnen Cities gehören
        private List<int> fromIndices;
        private List<int> toIndices;

        // Zeilenweise unterteilter Input-String
        private string[] input;

        // Wie oft wurde Next() schon aufgerufen?
        private int numberOfRuns;

        public SimpleObjectReader(StringReader stream)
        {
            deserializer = new Deserializer<City>();
            streamReader = stream;

            StringBuilder sb = new StringBuilder();

            // Für das Bestimmen der Teile des Input-Strings, welche zu einzelnen Cities gehören
            fromIndices = new List<int>();
            toIndices = new List<int>();
            int index = 0;
            int count = 0;
            int lastFromIndex = 0;
            Boolean setNewFromIndex = false;

            // Zeilenweises Auslesen des Input-Strings
            string tmp = streamReader.ReadLine();
            while (tmp != null)
            {
                if (tmp.StartsWith("Instance of"))
                {
                    // Mit "Instance of" kann ein neues City-Objekt anfangen,
                    // oder aber auch ein Property von irgendeinem anderen Typ als string/int/double
                    if (setNewFromIndex)
                    {
                        // Das letzte "End of instace" war tatsächlich das Ende eines City-Objekts
                        // Dieser Index ist Start-Index eines weiteren City-Objekts
                        lastFromIndex = index;
                        setNewFromIndex = false;
                    }
                    count++;
                }
                else if (tmp.Equals("End of instance"))
                {
                    // Mit "End of instance" kann ein neues City-Objekt enden,
                    // oder aber auch ein Property von irgendeinem anderen Typ als string/int/double
                    if (--count == 0)
                    {
                        // Tatsächliches Ende eines City-Objekts:
                        // Hinzufügen der Start- und End-Indizes zu den entsprechenden Listen
                        fromIndices.Add(lastFromIndex);
                        toIndices.Add(index);

                        // Sicherstellen, dass nächstes "Instance of" als Start eines neuen City-Objekts erkannt wird
                        setNewFromIndex = true;
                    }
                }

                index++;

                sb.Append(tmp + ";");
                tmp = streamReader.ReadLine();
            }

            // der Input für den Deserializer soll ein Array sein
            input = sb.ToString().Split(';');

            // Next() wurde noch nie aufgerufen
            numberOfRuns = 0;
        }

        public City Next()
        {
            // Sollte Next() öfters aufgerufen werden, als es City-Objekte im Input-String gibt,
            // soll 'null' zurückgegeben werden
            if (numberOfRuns == fromIndices.Count)
            {
                return null;
            }

            // Auslesen der richtigen Start- und End-Indizes für diesen Aufruf von Next()
            int fromIndex = fromIndices[numberOfRuns];
            int toIndex = toIndices[numberOfRuns];
            int length = toIndex - fromIndex + 1;

            // Den passenden Teil des Input-Strings herausnehmen für diesen Aufruf von Next()
            string[] newInput = new string[length];
            Array.Copy(input, fromIndex, newInput, 0, length);

            // Aufruf-Counter erhöhen!
            numberOfRuns++;

            // deserialisiertes City-Objekt vom Deserializer zurückgeben
            return deserializer.deserialize(newInput);
        }
    }
}
