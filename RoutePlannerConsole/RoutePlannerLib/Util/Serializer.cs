﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Reflection;

namespace Fhnw.Ecnf.RoutePlanner.RoutePlannerLib.Util
{
    public class Serializer<T>
    {

        public string serialize(T t)
        {
            
            StringBuilder sb = new StringBuilder();

            // Typ des zu serialisirenden Objekts
            Type type = t.GetType();
            string fullTypeName = type.FullName;
            sb.Append("Instance of ");
            sb.Append(fullTypeName);
            sb.Append("\r\n");

            // Properties des zu serialisirenden Objekts
            var properties = type.GetProperties();
            string propertyType;
            foreach (PropertyInfo propertyInfo in properties)
            {
                // Lab 9 (11): ignore the index property
                // -------------------------------------
                if (propertyInfo.Name == "Index")
                {
                    continue;
                }
                // -------------------------------------

                // Typ des Properties
                propertyType = propertyInfo.PropertyType.FullName;

                if (propertyType.Equals("System.String"))
                {
                    // String Werte mit Anführungs- und Schlusszeichen
                    sb.Append(propertyInfo.Name);
                    sb.Append("=\"");
                    sb.Append(propertyInfo.GetValue(t));
                    sb.Append("\"\r\n");
                } else if (propertyType.StartsWith("System.Int")){
                    // Integer Werte ohne Anführung- und Schlusszeichen
                    sb.Append(propertyInfo.Name);
                    sb.Append("=");
                    sb.Append(propertyInfo.GetValue(t).ToString());
                    sb.Append("\r\n");
                }
                else if (propertyType.Equals("System.Double"))
                {
                    // Double Werte ohne Anführungs- und Schlusszeichen
                    sb.Append(propertyInfo.Name);
                    sb.Append("=");

                    // Sicher gehen, dass '.' als Dezimalsymbol verwendet wird
                    double doubleValue;
                    Boolean isDouble = double.TryParse(propertyInfo.GetValue(t).ToString(), out doubleValue);
                    if (isDouble)
                    {
                        sb.Append(doubleValue.ToString(System.Globalization.CultureInfo.InvariantCulture));
                    }
                    else
                    {
                        // To Do: throw exception: error parsing double
                    }
                    sb.Append("\r\n");
                }
                else
                {
                    // Alle anderen Typen: "is a nested object..." hinzufügen und das Property "rekursiv" serialisieren lassen
                    sb.Append(propertyInfo.Name);
                    sb.Append(" is a nested object...\r\n");
                    Serializer<object> mySerializer = new Serializer<object>();
                    string nestedOutput = mySerializer.serialize(propertyInfo.GetValue(t));
                    sb.Append(nestedOutput);
                }
            }

            // Serialisierung beendet
            sb.Append("End of instance\r\n");

            return sb.ToString();
        }


    }
}
