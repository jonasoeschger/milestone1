﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace Fhnw.Ecnf.RoutePlanner.RoutePlannerLib.Util
{
    public class SimpleObjectWriter
    {

        // Serializer
        private Serializer<City> citySerializer;

        // Writer
        private StringWriter streamWriter;


        public SimpleObjectWriter(StringWriter stream)
        {
            citySerializer = new Serializer<City>();
            streamWriter = stream;
        }

        public void Next(City city)
        {
            // Output des Serializers zurückgeben
            string serializedCity = citySerializer.serialize(city);
            streamWriter.Write(serializedCity);

            // Writer nicht schliessen, Next() soll mehrmals nacheinander aufgerufen werden können!
            //streamWriter.Close();
        }

    }
}
