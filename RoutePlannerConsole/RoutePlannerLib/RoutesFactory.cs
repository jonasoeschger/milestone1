﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Reflection;

namespace Fhnw.Ecnf.RoutePlanner.RoutePlannerLib
{
    public class RoutesFactory {
        static public IRoutes Create(Cities cities)
        {
            string algorithmClassName = Properties.Settings.Default.RouteAlgorithm;

            return Create(cities, algorithmClassName);
        }
        static public IRoutes Create(Cities cities, string algorithmClassName)
        {
            Type type = Type.GetType(algorithmClassName);
            if (type == null)
            {
                return null;
            }
            else
            {
                return (IRoutes)Activator.CreateInstance(type, cities);
            }
        }
    }
}
