﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fhnw.Ecnf.RoutePlanner.RoutePlannerLib
{
    // Lab 3: Task 2a)
    // ---------------
    // store information when a route is requested and Routes.FindShortestRouteBetween fires RouteRequestHandler event
    public class RouteRequestEventArgs : System.EventArgs
    {

        public RouteRequestEventArgs(City fromCity, City toCity, TransportModes transportMode)
        {
            FromCity = fromCity;
            ToCity = toCity;
            TransportMode = transportMode;
        }

        public City FromCity { get; private set; }

        public City ToCity { get; private set; }

        public TransportModes TransportMode { get; private set; }

    }
}
