﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Dynamic;

namespace Fhnw.Ecnf.RoutePlanner.RoutePlannerLib.Dynamic
{
    public class World : DynamicObject
    {

        private Cities cities;

        public World(Cities cities)
        {
            this.cities = cities;
        }

        public override bool TryInvokeMember(InvokeMemberBinder binder, object[] args, out object result)
        {
            Console.WriteLine(binder.Name+ " method was called");

            City foundCity = cities.FindCity(binder.Name);
            if (foundCity != null)
            {
                result = foundCity;
            }
            else
            {
                result = "The city \"" + binder.Name + "\" does not exist!";
            }

            return true;
        }
    }
}
