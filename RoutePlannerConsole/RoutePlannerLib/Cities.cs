﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Diagnostics;

namespace Fhnw.Ecnf.RoutePlanner.RoutePlannerLib
{
    public class Cities
    {
        private static TraceSource traceSource = new TraceSource("Cities");

        private List<City> cityList = new List<City>();

        public City this[int index]
        {
            get
            {
                if ((index >= 0) && (index < cityList.Count))
                {
                    return cityList[index];
                }
                else
                {
                    return null;
                }
            }
        }

        public void Add(City city)
        {
            cityList.Add(city);
        }

        public int Count
        {
            get
            {
                return cityList.Count;
            }
        }

        public int ReadCities(string filename)
        {
            traceSource.TraceEvent(TraceEventType.Information, 1, "ReadCities started.");

            //TextReader tr = new StreamReader(filename);
            //int count = 0;
            //string line;
            //string[] input;
            //while ((line = tr.ReadLine()) != null)
            //{
            //    input = line.Split('\t');

            //    if (input.Count() == 5)
            //    {
            //        int population = Convert.ToInt32(input[2]);
            //        //double latitude = Convert.ToDouble(input[3]);
            //        //double longitude = Convert.ToDouble(input[4]);
            //        double latitude = Convert.ToDouble(double.Parse(input[3], System.Globalization.CultureInfo.InvariantCulture));
            //        double longitude = Convert.ToDouble(double.Parse(input[4], System.Globalization.CultureInfo.InvariantCulture));

            //        cityList.Add(new City(input[0], input[1], population, latitude, longitude));

            //        count++;
            //    }
            //}


            //return count;

            int count = 0;

            try
            {
                using (TextReader reader = new StreamReader(filename))
                {
                    IEnumerable<string[]> citiesAsStrings = reader.GetSplittedLines('\t');
                    //foreach (var cs in citiesAsStrings)
                    //{
                    //    cityList.Add(new City(cs[0].Trim(), cs[1].Trim(), int.Parse(cs[2]), double.Parse(cs[3], System.Globalization.CultureInfo.InvariantCulture), double.Parse(cs[4], System.Globalization.CultureInfo.InvariantCulture)));
                    //    count++;
                    //}
                    // Lab 6: Task 2)
                    // ------------------------------------------
                    var readCities = citiesAsStrings.Select(
                        x =>
                        new City(x[0].Trim(), x[1].Trim(), int.Parse(x[2]), double.Parse(x[3], System.Globalization.CultureInfo.InvariantCulture), double.Parse(x[4], System.Globalization.CultureInfo.InvariantCulture))
                    ).ToList();
                    //readCities.ForEach(x => cityList.Add(x));
                    cityList.AddRange(readCities);
                    count = readCities.Count();
                    // ------------------------------------------
                }
            }
            catch (FileNotFoundException e)
            {
                traceSource.TraceEvent(TraceEventType.Critical, 3, "File not found: " + filename+"\n"+e.StackTrace);
            }

            traceSource.TraceEvent(TraceEventType.Information, 2, "ReadCities ended.");

            return count;
            
        }

        
        //public List<City> FindNeighbours(WayPoint location, double distance)
        //{
        //    SortedList<double, City> sortedList = new SortedList<double, City>();

        //    foreach (City c in cityList) {

        //        double d = location.Distance(c.Location);
                
        //        if (d <= distance)
        //        {
        //            sortedList.Add(d, c);
        //        }
        //    }

        //    return new List<City>(sortedList.Values);
        //}     

        // Lab 6: Task 1)
        // ------------------------------------------
        public List<City> FindNeighbours(WayPoint location, double distance)
        {
            //return cityList.Where(x => location.Distance(x.Location) <= distance).ToList();
            //return cityList.OrderBy(x => location.Distance(x.Location)).Where(x => location.Distance(x.Location) <= distance).ToList();
            //return cityList
            //    .Select(city => new Tuple<City, double>(city, location.Distance(city.Location)))
            //    .Where(tuple => tuple.Item2 <= distance)
            //    .OrderBy(tuple => tuple.Item2)
            //    .Select(tuple => tuple.Item1).ToList();
            return cityList
                .Select(x => new { c = x, d = location.Distance(x.Location) })
                .Where(x => x.d <= distance)
                .OrderBy(x => x.d)
                .Select(x => x.c).ToList();
        }
        // ------------------------------------------

        // Lab 3: Task 1) --> Lab 6, Teil 1: Task 1a)
        // ------------------------------------------
        // return city with name cityName
        //public City FindCity(string cityName)
        //{
        //    return cityList.Find(delegate(City city)
        //    {
        //        //if (city.Name.ToLower() == cityName.ToLower()) return true;
        //        //return false;
        //        return string.Equals(city.Name, cityName, StringComparison.InvariantCultureIgnoreCase);
        //    });
        //}
        public City FindCity(string cityName)
        {
            return cityList.Find(c => string.Equals(c.Name, cityName.Trim(), StringComparison.InvariantCultureIgnoreCase));
        }
        // ---------------
        


        #region Lab04: FindShortestPath helper function
        /// <summary>
        /// Find all cities between 2 cities 
        /// </summary>
        /// <param name="from">source city</param>
        /// <param name="to">target city</param>
        /// <returns>list of cities</returns>
        public List<City> FindCitiesBetween(City from, City to)
        {
            var foundCities = new List<City>();
            if (from == null || to == null)
                return foundCities;

            foundCities.Add(from);

            var minLat = Math.Min(from.Location.Latitude, to.Location.Latitude);
            var maxLat = Math.Max(from.Location.Latitude, to.Location.Latitude);
            var minLon = Math.Min(from.Location.Longitude, to.Location.Longitude);
            var maxLon = Math.Max(from.Location.Longitude, to.Location.Longitude);

            // renames the name of the "cities" variable to your name of the internal City-List
            foundCities.AddRange(cityList.FindAll(c =>
                c.Location.Latitude > minLat && c.Location.Latitude < maxLat
                        && c.Location.Longitude > minLon && c.Location.Longitude < maxLon));

            foundCities.Add(to);

            // Lab 9 (11): Task 1)
            // -------------------
            foundCities = InitIndexForAlgorithm(foundCities);
            // -------------------

            return foundCities;
        }
        #endregion

        // Lab 9 (11): Task 1)
        // -------------------
        private List<City> InitIndexForAlgorithm(List<City> foundCities)
        {
            // set index for FloydWarshall
            for (int index = 0; index < foundCities.Count; index++)
            {
                foundCities[index].Index = index;
            }
            return foundCities;
        }
        // -------------------
    }
}
