﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Excel = Microsoft.Office.Interop.Excel;

namespace Fhnw.Ecnf.RoutePlanner.RoutePlannerLib.Export
{
    public class ExcelExchange
    {
        Excel.Application xlApp;

        public ExcelExchange()
        {
            try
            {
                xlApp = new Excel.Application();
            }
            catch (Exception)
            {
                Console.WriteLine("Could not instantiate Excel Application!");
            }
        }

        public int WriteToFile(String fileName, City from, City to, List<Link> links)
        {
            // check whether Excel is installed
            if (xlApp == null)
            {
                Console.WriteLine("Excel is not properly installed!!");
                return 1;
            }

            Excel.Workbook xlWorkBook;
            Excel.Worksheet xlWorkSheet;
            object misValue = System.Reflection.Missing.Value;
            xlWorkBook = xlApp.Workbooks.Add(misValue);

            // get work sheet
            xlWorkSheet = (Excel.Worksheet)xlWorkBook.Worksheets.get_Item(1);

            // set font and text of first row
            xlWorkSheet.Rows[1].Font.Size = 14;
            xlWorkSheet.Rows[1].Font.Bold = true;
            xlWorkSheet.Cells[1, 1] = "From";
            xlWorkSheet.Cells[1, 2] = "To";
            xlWorkSheet.Cells[1, 3] = "Distance";
            xlWorkSheet.Cells[1, 4] = "Transport Mode";

            // set text of next rows
            int index = 2;
            foreach (var l in links)
            {
                xlWorkSheet.Cells[index, 1] = l.FromCity.Name + " (" + l.FromCity.Country + ")";
                xlWorkSheet.Cells[index, 2] = l.ToCity.Name + " (" + l.ToCity.Country + ")";
                xlWorkSheet.Cells[index, 3] = l.Distance;
                xlWorkSheet.Cells[index, 4] = l.TransportMode.ToString();
                index++;
            }

            // set border of first row and make sure columns are adjusted automatically
            for (int i = 1; i <= 4; i++ )
            {
                xlWorkSheet.Columns[i].AutoFit();
                var border = xlWorkSheet.Cells[1, i].Borders;
                border.LineStyle = Excel.XlLineStyle.xlContinuous;
                border.Weight = 2d;
            }

            // save file without asking whether overriding is o.k.
            xlApp.DisplayAlerts = false;
            xlWorkBook.SaveAs(fileName, Excel.XlFileFormat.xlWorkbookNormal, misValue, misValue, misValue, misValue, Excel.XlSaveAsAccessMode.xlExclusive, misValue, misValue, misValue, misValue, misValue);
            xlWorkBook.Close(true, misValue, misValue);
            xlApp.Quit();

            Console.WriteLine("Successfully written to Excel-File: " + fileName);

            return 0;
        }
    }
}
