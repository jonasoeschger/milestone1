﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Threading;
using System.Linq;
using Fhnw.Ecnf.RoutePlanner.RoutePlannerLib;
using System.Diagnostics;
using System.Threading.Tasks;

namespace Fhnw.Ecnf.RoutePlanner.RoutePlannerLib
{
    public class RoutesDijkstra : Routes
    {
        public RoutesDijkstra(Cities cities) 
            : base(cities) 
        {

        }

        // Lab 10: Asynchronous
        // ---------------------

        // GoFindShortestRouteBetween calls "asynchronized" method FindShortestRouteBetweenAsync
        // optional parameter 'reportProgress': default value: null
        public async Task<List<Link>> GoFindShortestRouteBetween(string fromCity, string toCity, TransportModes mode, IProgress<string> reportProgress = null)
        {
            return await FindShortestRouteBetweenAsync(fromCity, toCity, mode, reportProgress);
        }

        // "asynchronized" method FindShortestRouteBetweenAsync runs the compute-bound task FindShortestRouteBetween
        public Task<List<Link>> FindShortestRouteBetweenAsync(string fromCity, string toCity, TransportModes mode, IProgress<string> reportProgress)
        {
            var task = Task.Run(() => FindShortestRouteBetween(fromCity, toCity, mode, reportProgress));
            return task;
        }

        // FindShortestRouteBetween with original signature without IProgress --> set to null
        public override List<Link> FindShortestRouteBetween(string fromCity, string toCity, TransportModes mode) 
        {
            return FindShortestRouteBetween(fromCity, toCity, mode, null); 
        }
        // ---------------------

        #region Lab04: Dijkstra implementation
        public List<Link> FindShortestRouteBetween(string fromCity, string toCity, TransportModes mode, IProgress<string> reportProgress)
        {
            // Lab 3: Task 2b)
            // ----------------

            // remark: "hack" with dummy cities if in cities, there is no city with name fromCity or toCity
            // but: don't add dem to cities !!
            City actualFromCity = cities.FindCity(fromCity);
            if (actualFromCity == null)
            {
                actualFromCity = new City(fromCity, "", 0, 0, 0);
                //cities.Add(actualFromCity);
            }
            City actualToCity = cities.FindCity(toCity);
            if (actualToCity == null)
            {
                actualToCity = new City(toCity, "", 0, 0, 0);
                //cities.Add(actualToCity);
            }

            NotifyObservers(actualFromCity, actualToCity, mode);

            var citiesBetween = FindCitiesBetween(fromCity, toCity);
            if (citiesBetween == null || citiesBetween.Count < 1 || routes == null || routes.Count < 1)
                return null;
            if (reportProgress != null)
            {
                reportProgress.Report("FindCitiesBetween done");
            }

            var source = citiesBetween[0];
            var target = citiesBetween[citiesBetween.Count - 1];

            Dictionary<City, double> dist;
            Dictionary<City, City> previous;
            var q = FillListOfNodes(citiesBetween, out dist, out previous);
            dist[source] = 0.0;
            if (reportProgress != null)
            {
                reportProgress.Report("FillListOfNodes done");
            }

            // the actual algorithm
            previous = SearchShortestPath(mode, q, dist, previous);
            if (reportProgress != null)
            {
                reportProgress.Report("SearchShortestPath done");
            }

            // create a list with all cities on the route
            var citiesOnRoute = GetCitiesOnRoute(source, target, previous);
            if (reportProgress != null)
            {
                reportProgress.Report("GetCitiesOnRoute done");
            }

            // prepare final list if links
            var foundPaths = FindPath(citiesOnRoute, mode);
            if (reportProgress != null)
            {
                reportProgress.Report("FindPath done");
            }

            return foundPaths;
        }

        private static List<City> FillListOfNodes(List<City> cities, out Dictionary<City, double> dist, out Dictionary<City, City> previous)
        {
            var q = new List<City>(); // the set of all nodes (cities) in Graph ;
            dist = new Dictionary<City, double>();
            previous = new Dictionary<City, City>();
            foreach (var v in cities)
            {
                dist[v] = double.MaxValue;
                previous[v] = null;
                q.Add(v);
            }

            return q;
        }

        /// <summary>
        /// Searches the shortest path for cities and the given links
        /// </summary>
        /// <param name="mode">transportation mode</param>
        /// <param name="q"></param>
        /// <param name="dist"></param>
        /// <param name="previous"></param>
        /// <returns></returns>
        private Dictionary<City, City> SearchShortestPath(TransportModes mode, List<City> q, Dictionary<City, double> dist, Dictionary<City, City> previous)
        {
            while (q.Count > 0)
            {
                City u = null;
                var minDist = double.MaxValue;
                // find city u with smallest dist
                foreach (var c in q)
                    if (dist[c] < minDist)
                    {
                        u = c;
                        minDist = dist[c];
                    }

                if (u != null)
                {
                    q.Remove(u);
                    foreach (var n in FindNeighbours(u, mode))
                    {
                        var l = FindLink(u, n, mode);
                        var d = dist[u];
                        if (l != null)
                            d += l.Distance;
                        else
                            d += double.MaxValue;

                        if (dist.ContainsKey(n) && d < dist[n])
                        {
                            dist[n] = d;
                            previous[n] = u;
                        }
                    }
                }
                else
                    break;

            }

            return previous;
        }


        /// <summary>
        /// Finds all neighbor cities of a city. 
        /// </summary>
        /// <param name="city">source city</param>
        /// <param name="mode">transportation mode</param>
        /// <returns>list of neighbor cities</returns>
        private List<City> FindNeighbours(City city, TransportModes mode)
        {
            var neighbors = new List<City>();
            foreach (var r in routes)
                if (mode.Equals(r.TransportMode))
                {
                    if (city.Equals(r.FromCity))
                        neighbors.Add(r.ToCity);
                    else if (city.Equals(r.ToCity))
                        neighbors.Add(r.FromCity);
                }

            return neighbors;
        }

        private List<City> GetCitiesOnRoute(City source, City target, Dictionary<City, City> previous)
        {
            var citiesOnRoute = new List<City>();
            var cr = target;
            while (previous[cr] != null)
            {
                citiesOnRoute.Add(cr);
                cr = previous[cr];
            }
            citiesOnRoute.Add(source);

            citiesOnRoute.Reverse();
            return citiesOnRoute;
        }
        #endregion


    }
        
}
