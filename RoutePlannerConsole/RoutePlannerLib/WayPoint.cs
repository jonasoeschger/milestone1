﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fhnw.Ecnf.RoutePlanner.RoutePlannerLib
{
    public class WayPoint
    {
        public string Name { get; set; } 
        public double Longitude { get; set; } 
        public double Latitude { get; set; }
         
        private int radius = 6371;

        public WayPoint() { }
        
        public WayPoint(string _name, double _latitude, double _longitude) 
        { 
            Name = _name; 
            Latitude = _latitude; 
            Longitude = _longitude;
        }

        public override string ToString()
        {
            string name = "";
            string separator = "";
            if (Name != null)
            {
                name = Name;
                if (Name != "")
                {
                    separator = " ";
                }
            }
            //return "WayPoint: " + name + separator + Math.Round(Latitude, 2) + "/" + Math.Round(Longitude, 2);
            return "WayPoint: " + name + separator + string.Format("{0:F2}", Latitude) + "/" + string.Format("{0:F2}", Longitude);
        }

        public double Distance(WayPoint target)
        {
            double lat1 = Latitude * Math.PI / 180;
            double lat2 = target.Latitude * Math.PI / 180;
            double long1 = Longitude * Math.PI / 180;
            double long2 = target.Longitude * Math.PI / 180;

            double d1 = Math.Sin(lat1) * Math.Sin(lat2);
            double d2 = Math.Cos(lat1) * Math.Cos(lat2) * Math.Cos(long1 - long2);

            return radius * Math.Acos(d1 + d2);
        }

        public static WayPoint operator +(WayPoint lhs, WayPoint rhs)
        {
            return new WayPoint(lhs.Name, lhs.Latitude + rhs.Latitude, lhs.Longitude + rhs.Longitude);
        }

        public static WayPoint operator -(WayPoint lhs, WayPoint rhs)
        {
            return new WayPoint(lhs.Name, lhs.Latitude - rhs.Latitude, lhs.Longitude - rhs.Longitude);
        }
    }
}
