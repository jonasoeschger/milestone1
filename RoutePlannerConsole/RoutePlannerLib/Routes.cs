
using System;
using System.IO;
using System.Collections.Generic;
using System.Threading;
using System.Linq;
using Fhnw.Ecnf.RoutePlanner.RoutePlannerLib;
using System.Diagnostics;

namespace Fhnw.Ecnf.RoutePlanner.RoutePlannerLib
{
    /// <summary>
    /// Manages a routes from a city to another city.
    /// </summary>
    public abstract class Routes : IRoutes
    {
        private static TraceSource traceSource = new TraceSource("Routes");

        protected List<Link> routes = new List<Link>();
        protected Cities cities;

        // Lab 3: Task 2a)
        // ---------------
        // delegate and event to observe FindShortestRouteBetween
        public delegate void RouteRequestHandler(object sender, RouteRequestEventArgs e);
        public event RouteRequestHandler RouteRequestEvent;
        // ---------------

        // Lab 9 (11): Task 1)
        // -------------------
        public abstract List<Link> FindShortestRouteBetween(string fromCity, string toCity, TransportModes mode);
        public bool ExecuteParallel { set; get; }
        // -------------------

        public int Count
        {
            get { return routes.Count; }
        }

        /// <summary>
        /// Initializes the Routes with the cities.
        /// </summary>
        /// <param name="cities"></param>
        public Routes(Cities cities)
        {
            this.cities = cities;
        }

        /// <summary>
        /// Reads a list of links from the given file.
        /// Reads only links where the cities exist.
        /// </summary>
        /// <param name="filename">name of links file</param>
        /// <returns>number of read route</returns>
        public int ReadRoutes(string filename)
        {
            traceSource.TraceEvent(TraceEventType.Information, 1, "ReadRoutes started.");

            //using (TextReader reader = new StreamReader(filename))
            //{
            //    string line;
            //    while ((line = reader.ReadLine()) != null)
            //    {
            //        var linkAsString = line.Split('\t');

            //        City city1 = cities.FindCity(linkAsString[0]);
            //        City city2 = cities.FindCity(linkAsString[1]);

            //        // only add links, where the cities are found 
            //        if ((city1 != null) && (city2 != null))
            //        {
            //            routes.Add(new Link(city1, city2, city1.Location.Distance(city2.Location), TransportModes.Rail));
            //        }
            //    }
            //}
            //return Count;

            using (TextReader reader = new StreamReader(filename))
            {
                IEnumerable<string[]> routesAsStrings = reader.GetSplittedLines('\t');
                //foreach (var rs in routesAsStrings)
                //{
                //    City city1 = cities.FindCity(rs[0]);
                //    City city2 = cities.FindCity(rs[1]);

                //    // only add links, where the cities are found 
                //    if ((city1 != null) && (city2 != null))
                //    {
                //        routes.Add(new Link(city1, city2, city1.Location.Distance(city2.Location), TransportModes.Rail));
                //    }
                //}

                // Lab 6: Task 4)
                // ------------------------------------------
                var routesReadIn = routesAsStrings.Select(x => new { city1 = cities.FindCity(x[0]), city2 = cities.FindCity(x[1]) });
                var routesOfExistingCities = routesReadIn.Where(x => (x.city1 != null) && (x.city2 != null));
                routesOfExistingCities.ToList().ForEach(
                    x => routes.Add(new Link(x.city1, x.city2, x.city1.Location.Distance(x.city2.Location), TransportModes.Rail))
                );
                // ------------------------------------------
            }

            traceSource.TraceEvent(TraceEventType.Information, 2, "ReadRoutes ended.");

            return Count;

        }

        // Lab 4: ben�tigte Methoden f�r FindShortestRouteBetween
        // ------------------------------------------------------

        protected List<City> FindCitiesBetween(string fromCity, string toCity)
        {
            return cities.FindCitiesBetween(cities.FindCity(fromCity), cities.FindCity(toCity));
        }

        protected Link FindLink(City c1, City c2, TransportModes mode)
        {
            //foreach (Link link in routes)
            //{
            //    if (link.FromCity.Equals(c1))
            //    {
            //        if (link.ToCity.Equals(c2))
            //        {
            //            if (link.TransportMode == mode)
            //            {
            //                return link;
            //            }
            //        }
            //    }

            //    if (link.FromCity.Equals(c2))
            //    {
            //        if (link.ToCity.Equals(c1))
            //        {
            //            if (link.TransportMode == mode)
            //            {
            //                return new Link(link.ToCity, link.FromCity, link.Distance, link.TransportMode);
            //            }
            //        }
            //    }
            //}
            //return null;

            // Lab 6: Task 4)
            // ------------------------------------------
            var correctMode = routes.Where(x => x.TransportMode == mode);
            var oneDirection = correctMode.Where(x => x.FromCity.Equals(c1) && x.ToCity.Equals(c2));
            var otherDirection = correctMode.Where(x => x.FromCity.Equals(c2) && x.ToCity.Equals(c1));

            if (oneDirection.Count() > 0)
            {
                return oneDirection.First();
            }
            else if (otherDirection.Count() > 0)
            {
                return new Link(otherDirection.First().ToCity, otherDirection.First().FromCity, otherDirection.First().Distance, otherDirection.First().TransportMode);
            }
            else
            {
                return null;
            }
            // ------------------------------------------
        }

        protected List<Link> FindPath(List<City> cityList, TransportModes mode)
        {
            List<Link> linkList = new List<Link>();

            for (int i = 0; i < cityList.Count - 1; i++)
            {
                linkList.Add(FindLink(cityList.ElementAt(i), cityList.ElementAt(i + 1), mode));
            }

            return linkList;
        }

        // ------------------------------------------------------


        // Lab 6: Task 3) St�dte in mindestens einer Route mit richtigem Transport Mode
        // ----------------------------------------------------------------------------
        public City[] FindCities(TransportModes transportMode)
        {
            var correctMode = routes.Where(x => x.TransportMode == transportMode);
            var fromCities = correctMode.Select(x => x.FromCity);
            var toCities = correctMode.Select(x => x.ToCity);
            var allCities = fromCities.Union(toCities);
            var distinctCities = allCities.Distinct();

            return distinctCities.ToArray();
        }
        // -----------------------------------------------------

        // Lab 9 (11): Task 1) Schritt 3: separate Methode NotifyObservers 
        // ----------------------------------------------------------------------------
        protected void NotifyObservers(City actualFromCity, City actualToCity, TransportModes mode)
        {
            if (RouteRequestEvent != null)
            {
                RouteRequestEvent(this, new RouteRequestEventArgs(actualFromCity, actualToCity, mode));
            }
        }
    }
}
