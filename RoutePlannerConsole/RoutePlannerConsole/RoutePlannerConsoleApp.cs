﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Reflection;
using System.Runtime.CompilerServices;
using Fhnw.Ecnf.RoutePlanner.RoutePlannerLib;
using Fhnw.Ecnf.RoutePlanner.RoutePlannerLib.Export;
using Fhnw.Ecnf.RoutePlanner.RoutePlannerLib.Dynamic;

namespace Fhnw.Ecnf.RoutePlanner.RoutePlannerConsole
{
    class RoutePlannerConsoleApp
    {
        static void Main(string[] args)
        {
            // Hello Message
            // -------------------------
            var assembly = Assembly.GetExecutingAssembly();
            string version = assembly.GetName().Version.ToString();
            Console.WriteLine("Welcome to RoutePlanner ({0})", version);

            // Print Windisch
            // -------------------------
            var wayPoint = new WayPoint("Windisch", 47.479319847061966, 8.212966918945312);
            Console.WriteLine("{0}: {1}/{2}", wayPoint.Name, wayPoint.Latitude, wayPoint.Longitude);

            // Test WayPoint.ToString()
            // -------------------------
            //var dummyWayPoint1 = new WayPoint(null, 47.479319847061966, 8.212966918945312);
            //var dummyWayPoint2 = new WayPoint("", 47.479319847061966, 8.212966918945312);
            //Console.WriteLine(wayPoint.ToString());
            //Console.WriteLine(dummyWayPoint1.ToString());
            //Console.WriteLine(dummyWayPoint2.ToString());

            // Test WayPoint.Distance()
            // -------------------------
            //var wayPointBern = new WayPoint("Bern", 46.9479222, 7.444608499999958);
            //var wayPointTripolis = new WayPoint("Tripolis", 32.8084124, 13.150967199999968);
            //double distance = wayPointBern.Distance(wayPointTripolis);
            //double distance1 = wayPointTripolis.Distance(wayPointBern);
            //Console.WriteLine(distance);
            //Console.WriteLine(distance1);

            // Test Cities.ReadCities()
            // -------------------------
            //var cities = new Cities();
            ////cities.ReadCities(@"C:\Temp\citiesTestDataLab2.txt");
            //cities.ReadCities(@"C:\Temp\citiesTestDataLab3.txt");
            //for (int i = 0; i < cities.Count; i++)
            //{
            //    Console.WriteLine("City: {0} {1} {2} {3} {4}", cities[i].Name, cities[i].Country, cities[i].Population, cities[i].Location.Latitude, cities[i].Location.Longitude);
            //    Console.WriteLine(cities[i].Location.ToString());
            //}

            // Test Cities.FindNeighbours()
            // -----------------------------
            //var cities = new Cities();
            //var res = cities.FindNeighbours(wayPoint, 7000);
            //foreach (City c in res)
            //{
            //    Console.WriteLine(c.Location.ToString());
            //}

            // Test observing of Routes.FindShortestRouteBetween
            // --------------------------------------------------
            //var reqWatch = new RouteRequestWatcher();

            //var routes = new Routes(cities);

            //routes.RouteRequestEvent += reqWatch.LogRouteRequests;

            //routes.FindShortestRouteBetween("Bern", "Zürich", TransportModes.Rail);
            //routes.FindShortestRouteBetween("Bern", "Zürich", TransportModes.Car);
            //routes.FindShortestRouteBetween("Basel", "Bern", TransportModes.Rail);

            // Test ExcelExchange
            //var bern = new City("Bern", "Switzerland", 5000, 46.95, 7.44);
            //var zuerich = new City("Zürich", "Switzerland", 100000, 32.876174, 13.187507);
            //var aarau = new City("Aarau", "Switzerland", 10000, 35.876174, 12.187507);
            //var link1 = new Link(bern, aarau, 15, TransportModes.Ship);
            //var link2 = new Link(aarau, zuerich, 20, TransportModes.Ship);
            //var links = new List<Link>();
            //links.Add(link1);
            //links.Add(link2);

            //var fileName = @"C:\Temp\myExcel.xls";

            //ExcelExchange exchange = new ExcelExchange();
            //exchange.WriteToFile(fileName, bern, zuerich, links);

            // Test DynamicObject
            //var cities = new Cities();
            //cities.ReadCities("data/citiesTestDataLab2.txt");
            //dynamic world = new World(cities);
            //dynamic karachi = world.Karachi();
            //Console.WriteLine(karachi.Name);
            //string notFound = world.Entenhausen();
            //Console.WriteLine(notFound);

            // Test Trace
            // ----------
            // read cities from existing file
            var cities = new Cities();
            cities.ReadCities("data/citiesTestDataLab4.txt");
            // read routes from existing file
            var routes = new RoutesDijkstra(cities);
            routes.ReadRoutes("data/linksTestDataLab4.txt");
            // read cities from non existing file --> FileNotFoundException logged
            cities.ReadCities("nonExistingFile.txt");
                        
            Console.ReadKey();
        }
    }
}
