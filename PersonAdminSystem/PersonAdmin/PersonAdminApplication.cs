﻿using PersonAdminLib;
using System;
using System.Reflection;

namespace PersonAdmin
{
    struct A
    {
        public int x;
    }

    /// <summary>
    /// asdjasldjk
    /// </summary>
    class PersonAdminApplication
    {

        static void Main()
        {
            A x = new A();
            A y = x;
            Test(x);
            Console.WriteLine(x.x);

            /*Console.WriteLine("My first C# Program: {0}", 
                Assembly.GetExecutingAssembly().GetName().Version);
            */
            var person = new Person("Hans", "Müller");
//            Console.WriteLine("Person: {0} {1}", person.GetFirstname(), person.GetSurname());
            Console.WriteLine("Person: {0} {1}", person.Firstname, person.Surname);

            var personRegister = new PersonRegister();
            personRegister.Add(person);
            personRegister.Add(new Person("Sepp", "Meier"));
            personRegister.Add(new Person("Frank", "Bischof"));
            //Console.WriteLine("Person: {0} {1}", personRegister[0].Firstname, personRegister[0].Surname);
            //Console.WriteLine("Person: {0} {1}", personRegister[1].Firstname, personRegister[1].Surname);

            //Console.WriteLine("Person: {0} {1}", personRegister[personRegister.Count - 1].Firstname, personRegister[personRegister.Count - 1].Surname);

            //personRegister.ReadPersonsFromFile(@"C:\Temp\Persons.txt");
            //for (int i = 0; i < personRegister.Count; i++ )
            //{
            //    Console.WriteLine("Person: {0} {1}", personRegister[i].Firstname, personRegister[i].Surname);
            //}

            //Console.WriteLine("Press any key to finish");
            //Console.ReadKey();

            // Sort using Delegates
            Console.WriteLine("\nPersonen:");
            PrintPersons(personRegister);

            Comparison<Person> firstnameComparer = delegate(Person p1, Person p2)
            {
                return p1.Firstname.CompareTo(p2.Firstname);
            };

            Comparison<Person> surnameComparer = delegate(Person p1, Person p2)
            {
                return p1.Surname.CompareTo(p2.Surname);
            };

            Console.WriteLine("\nNach Voranmen sortiert:");
            personRegister.Sort(firstnameComparer);
            PrintPersons(personRegister);

            Console.WriteLine("\nNach Nachnamen sortiert:");
            personRegister.Sort(surnameComparer);
            PrintPersons(personRegister);

            // Events / Observer
            PersonRegister.PersonAddedHandler handler1 = new PersonRegister.PersonAddedHandler(PrintAddedPerson);
            PersonRegister.PersonAddedHandler handler2 = new PersonRegister.PersonAddedHandler(PrintAddedPersonWithDateAndTime);
            personRegister.PersonAdded += handler1;
            personRegister.PersonAdded += handler2;

            personRegister.AddPerson(new Person("Max", "Muster"));

            // Predicates
            String surname = "Meier";
            Person foundPerson1 = personRegister.FindPersonWithPredicate(delegate(Person p)
            {
                if (p.Surname == surname.Trim()) return true;
                return false;
            });

            if (foundPerson1 != null)
            {
                Console.WriteLine("\nGefundene Person: {0}, {1}", foundPerson1.Firstname, foundPerson1.Surname);
            }
            else
            {
                Console.WriteLine("\nPerson nicht gefunden.");
            }

            String firstname = "Hans";
            Person foundPerson2 = personRegister.FindPersonWithPredicate(delegate(Person p)
            {
                if (p.Firstname == firstname.Trim()) return true;
                return false;
            });

            if (foundPerson2 != null)
            {
                Console.WriteLine("\nGefundene Person: {0}, {1}", foundPerson2.Firstname, foundPerson2.Surname);
            }
            else
            {
                Console.WriteLine("\nPerson nicht gefunden.");
            }

            Console.WriteLine("\nPress any key to finish");
            Console.ReadKey();
        }

        private static void Test(A x)
        {
            x.x = 7;
        }

        private static void PrintPersons(PersonRegister personRegister)
        {
            foreach (var p in personRegister.Persons)
                Console.WriteLine("{0} {1}", p.Surname, p.Firstname);
        }

        private static void PrintAddedPerson(Person p)
        {
            Console.WriteLine("\nPerson hinzugefügt: {0}, {1}", p.Firstname, p.Surname);
        }

        private static void PrintAddedPersonWithDateAndTime(Person p)
        {
            Console.WriteLine("\nPerson hinzugefügt: {0}, {1} - {2}", p.Firstname, p.Surname, DateTime.Now);
        }

    }
}
