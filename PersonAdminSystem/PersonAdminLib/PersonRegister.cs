﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO; 

namespace PersonAdminLib
{
    public class PersonRegister
    {

        public delegate void PersonAddedHandler(Person newPerson);
        public event PersonAddedHandler PersonAdded;

        private List<Person> personList;

        public PersonRegister()
        {
            personList = new List<Person>();
        }

        public Person this[int index]
        {
            get
            {
                return personList[index];
            }
        }

        public void Add(Person person)
        {
            personList.Add(person);
        }

        public int Count
        {
            get
            {
                return personList.Count;
            }
        }

        public int ReadPersonsFromFile(string filename)
        {
            TextReader tr = new StreamReader(filename);

            String line;
            String[] names;
            while ((line = tr.ReadLine()) != null)
            {
                names = line.Split('\t');
                personList.Add(new Person(names[0], names[1]));
            }


            return 0;
        }

        public List<Person> Persons
        {
            get
            {
                return personList;
            }
        }

        public void Sort(Comparison<Person> c)
        {
            personList.Sort(c);
        }

        public int AddPerson(Person newPerson)
        {
            personList.Add(newPerson);

            if (PersonAdded != null)
            {
                PersonAdded(newPerson);
            }

            return personList.Count;
        }

        public Person FindPerson(string surname)
        {
            foreach (var p in personList)
                if (p.Surname == surname.Trim())
                    return p;
            return null;
        }

        public Person FindPersonWithPredicate(Predicate<Person> pred)
        {
            return personList.Find(pred);
        }
    }
}