﻿namespace PersonAdminLib
{
    /// <summary>
    /// Represents a Person data object
    /// </summary>
    public class Person
    {
//        private string firstname;
//        private string surname;

        /// <summary>
        /// Initializes Person data object
        /// For keeping code simple NO argument checks 
        /// </summary>
        /// <param name="firstname">can also be null</param>
        /// <param name="surname"></param>
        public Person(string firstname, string surname)
        {
//            this.firstname = firstname;
//            this.surname = surname;
            Firstname = firstname;
            Surname = surname;
        }

        public string Firstname { get; set; }
        public string Surname { get; set; }

//        public string GetFirstname() { return firstname ; }
//        public string GetSurname()
//        {
//            return surname;
//        }
    }
}